# IT Security Tutorship Slides at TUM

Example animation slides that I created during my tenure as an IT security tutor at the Technical University of Munich (TUM). These slides were used to facilitate discussions and learning in various topics related to IT security.

## About the Slides

- Kerberos, Block Ciphers, WEP
